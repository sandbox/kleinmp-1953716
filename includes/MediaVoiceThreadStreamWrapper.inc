<?php

/**
 *  @file media_voicethread/includes/MediaVoiceThreadStreamWrapper.inc
 *
 *  Create a VoiceThread Stream Wrapper class for the Media/Resource module.
 */

/**
 *  Create an instance like this:
 *  $voicethread = new MediaVoiceThreadStreamWrapper('voicethread://b/[video-code]');
 */
class MediaVoiceThreadStreamWrapper extends MediaReadOnlyStreamWrapper {

  // Overrides $base_url defined in MediaReadOnlyStreamWrapper.
  protected $base_url = 'http://voicethread.com/book.swf';

  /**
   * Returns a url in the format "http://voicethread.com/book.swf?b=123456".
   *
   * Overrides interpolateUrl() defined in MediaReadOnlyStreamWrapper.
   * This is an exact copy of the function in MediaReadOnlyStreamWrapper,
   * here in case that example is redefined or removed.
   */
  function interpolateUrl() {
    if ($parameters = $this->get_parameters()) {
      return $this->base_url . '?' . http_build_query($parameters);
    }
  }

  static function getMimeType($uri, $mapping = NULL) {
    return 'video/voicethread';
  }

  /**
   * @todo: Is this necessary?
   */
  function getTarget($f) {
    return FALSE;
  }

  /**
   * Get the path of the thumbnail file.
   *
   * VoiceThread does not have thumbnail support so we pass the
   * voicethread default icon as the thumbnail for all items.
   *
   * @return string
   *  The local path to the file.
   */
  function getLocalThumbnailPath() {
    $filename = 'vt-logo.png';
    $local_path = file_default_scheme() . '://media-voicethread/' . $filename;
    $current_path = drupal_get_path('module', 'media_voicethread') . '/' . $filename;

    // Copy the file over if it does not exist so it can support image styles.
    if (!file_exists($local_path)) {
      $dirname = drupal_dirname($local_path);
      file_prepare_directory($dirname, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
      @copy($current_path, $local_path);
    }
    return $local_path;
  }
}
