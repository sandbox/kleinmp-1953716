<?php

/**
 * @file media_voicethread/includes/themes/media_voicethread.theme.inc
 *
 * Theme and preprocess functions for Media: VoiceThread.
 */

/**
 * Preprocess function for theme('media_voicethread_video').
 */
function media_voicethread_preprocess_media_voicethread_video(&$variables) {

  // Build the URI.
  $wrapper = file_stream_wrapper_get_instance_by_uri($variables['uri']);
  $parts = $wrapper->get_parameters();
  $variables['video_id'] = check_plain($parts['b']);

  // Make the file object available.
  $file_object = file_uri_to_object($variables['uri']);

  // Parse options and build the query string.
  $query = array();
  $query['b'] = $variables['video_id'];

  // Non-query options.
  $url_base = 'voicethread.com';

  // Add some options as their own template variables.
  foreach (array('width', 'height') as $theme_var) {
    $variables[$theme_var] = $variables['options'][$theme_var];
  }

  // Turn on autoplay if enabled.
  if (!empty($variables['options']['autoplay'])) {
    $query['autoplay'] = 1;
  }

  // Do something useful with the overridden attributes from the file
  // object. We ignore alt and style for now.
  if (isset($variables['options']['attributes']['class'])) {
    if (is_array($variables['options']['attributes']['class'])) {
      $variables['classes_array'] = array_merge($variables['classes_array'], $variables['options']['attributes']['class']);
    }
    else {
      // Provide nominal support for Media 1.x.
      $variables['classes_array'][] = $variables['options']['attributes']['class'];
    }
  }

  // Add template variables for accessibility.
  $variables['title'] = check_plain($file_object->filename);
  // @TODO: Should we display this?
  $variables['alternative_content'] = t('Video of @title', array('@title' => $variables['title']));

  // Build the object URL with options query string.
  $variables['url'] = url('https://' . $url_base . '/book.swf', array('query' => $query, 'external' => TRUE));
}
