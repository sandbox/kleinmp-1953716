<?php

/**
 * @file media_youtube/includes/themes/media-youtube-video.tpl.php
 *
 * Template file for theme('media_voicethread_video').
 *
 * Variables available:
 *  $uri - The media uri for the VoiceThread video (e.g., voicethread://b/123456).
 *  $video_id - The unique identifier of the VoiceThread video (e.g., 23456).
 *  $id - The file entity ID (fid).
 *  $url - The full url including query options for the VoiceThread iframe.
 *  $options - An array containing the Media VoiceThread formatter options.
 *  $api_id_attribute - An id attribute if the Javascript API is enabled;
 *  otherwise NULL.
 *  $width - The width value set in Media: VoiceThread file display options.
 *  $height - The height value set in Media: VoiceThread file display options.
 *  $title - The Media: VoiceThread file's title.
 *
 */

?>
<object height="<?php print $height; ?>" width="<?php print $width; ?>">
  <param name="movie" value="<?php print $url; ?>" />
  <param name="wmode" value="transparent"/>
  <embed src="<?php print $url; ?>" type="application/x-shockwave-flash" wmode="transparent" width="<?php print $width; ?>" height="<?php print $height; ?>"></embed>
</object>
