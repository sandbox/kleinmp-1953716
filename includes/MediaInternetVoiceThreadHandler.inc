<?php

/**
 * @file media_voicethread/includes/MediaInterenetVoiceThreadHandler.inc
 *
 * Contains MediaInternetVoiceThreadHandler.
 */

/**
 * Implementation of MediaInternetBaseHandler.
 *
 * @see hook_media_internet_providers().
 */
class MediaInternetVoiceThreadHandler extends MediaInternetBaseHandler {
  /**
   * Check if a VoiceThread video id is valid.
   *
   * Check against the oembed stream instead of the gdata api site to
   * avoid "yt:quota too_many_recent_calls" errors.
   *
   * @return
   *   Boolean.
   */
  static public function validId($id) {
    $url = 'http://voicethread.com/book.swf?b=' . $id;
    $response = drupal_http_request($url, array('method' => 'HEAD'));
    if ($response->code != 200) {
      throw new MediaInternetValidationException("The VoiceThread video ID is invalid or the video was deleted.");
    }
    return TRUE;
  }

  public function parse($embedCode) {
    $patterns = array(
      '@voicethread\.com/book.swf\?b=(\d+)@i',
      '@voicethread\.com/share/(\d+)@i',
    );
    foreach ($patterns as $pattern) {
      preg_match($pattern, $embedCode, $matches);
      // @TODO: Parse is called often. Refactor so that valid ID is checked
      // when a video is added, but not every time the embedCode is parsed.
      if (isset($matches[1]) && self::validId($matches[1])) {
        return file_stream_wrapper_uri_normalize('voicethread://b/' . $matches[1]);
      }
    }
  }

  public function claim($embedCode) {
    if ($this->parse($embedCode)) {
      return TRUE;
    }
  }

  public function getFileObject() {
    $uri = $this->parse($this->embedCode);
    $file = file_uri_to_object($uri, TRUE);

    if (empty($file->fid) && $info = $this->getOEmbed()) {
      $file->filename = truncate_utf8($info['title'], 255);
    }

    return $file;
  }

  /**
   * Returns information about the media. See http://video.search.yahoo.com/mrss.
   *
   * @return
   */
  public function getMRSS() {
    return FALSE;
  }

  /**
   * Returns information about the media. See http://www.oembed.com/.
   *
   * @return
   */
  public function getOEmbed() {
    $uri = $this->parse($this->embedCode);
    $parts = explode('/', $uri);
    $id = trim(array_pop($parts));
    return array(
      'url' => $this->embedCode,
      'title' => t('VoiceThread Video @id', array('@id' => $id)),
    );
  }
}
